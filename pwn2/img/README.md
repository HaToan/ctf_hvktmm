## Exploit PWN2
### Hàm Check()

![check](img/check.PNG)

* Hàm check có hai vòng lặp thực hiện đọc vào hai biến  cho đến khi gặp ký  tự "\n"  or  byte "\x00"
--> Từ đó có thể kết luận  là  chương trình bị Ouffer overflow trên stack
* Biến "s2" nếu chứa wordpass thì sẽ thực hiện hàm goisystem (một  hàm khá thuần việt)

### Hàm goisystem

![goisystem](img/gosytem.PNG)

* Tương tưởng dẽ nhai nhưng không? Tại hàm goisystem  có đoạn kiểm tra  `a1 = 1259264` thì mới tới đỉnh được
* Tư duy chút không thể dung 2 biến v2 và s2 để ghi đè biến  a1 được  bởi  vì khi vào hàm  thanh ghi esp  sẽ được trừ đi  8h  `sub     esp, 18h` (có nghĩa  là địa chỉ của bbiến a1 thấp hơn  hai biến v2 và s2 2
* !Nghĩ cách khác để có thể set được biến  `a1 = 1259264# 0x133700`  là  cho địa chỉ  v2, s2 thấp hơn biến a1  là sử dụng lệnh leave

```
; leave
mov esp, ebp
pop ebp
```

* Như vậy ta sẽ ROP tới địa chỉ  0x0804852C

![rop](img/asm_gosystem.PNG)

* Khi đó a1 sẽ có địa chỉ cao hơn biến v2,s2 từ đó ta có thể ghi đè biến a1 = 0x133700
* Nhưng không giá trị lại chứa byte null mà chương trình lại không cho phép  đọc  byte null

```
  do
  {
    read(0, &t, 1u);
    v2[i++] = t;
  }
  while ( t != 10 && t );
  v2[i - 1] = 0;
  i = 0;
  do
  {
    read(0, &t, 1u);
    s2[i++] = t;
  }
  while ( t != 10 && t );
  s2[i - 1] = 0;
```

### Nghĩ cách ghi đè biến a1 với byte null

* Để ý một chút `s2[i - 1] = 0;`  thì sau mỗi vòng lặp  read  chuỗi sẽ được gán byte \x00 ở cuối.
* Chốt hạ: là v2 = sẽ giá trị cho  a1 là 0x133711 và v2  sẽ  thay thế 0x11  = 0x00   ``s2[i - 1] = 0;
* Vậy là ta đã gọi dk hàm system với tham sô là "sh"

### Cat Flag

[Exploit.py](exploit.py)