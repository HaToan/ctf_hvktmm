## PWN1

### Hàm main

![main](img/main.PNG)

### memcmp()

```
NAME         top

       memcmp - compare memory areas
SYNOPSIS         top

       #include <string.h>

       int memcmp(const void *s1, const void *s2, size_t n);
DESCRIPTION         top

       The memcmp() function compares the first n bytes (each interpreted as
       unsigned char) of the memory areas s1 and s2.
RETURN VALUE         top

       The memcmp() function returns an integer less than, equal to, or
       greater than zero if the first n bytes of s1 is found, respectively,
       to be less than, to match, or be greater than the first n bytes of
       s2.

       For a nonzero return value, the sign is determined by the sign of the
       difference between the first pair of bytes (interpreted as unsigned
       char) that differ in s1 and s2.

       If n is zero, the return value is zero.
```

* Đại loại như sau: memcmp() là hàm so sánh n byte đầu tiên đối với hai biến s1, s2. Giá trị trả về nhỏ hơn, bằng, hoặc lớn  0

```
  read(0, &s, 0x100u);
  printf("secrect: %s", &s);
  v3 = strlen(&s);
  if ( !memcmp(&s, "KhongGiongTrenServer", v3) )
    system("/bin/sh");
```

* Kết luận bạn nhập thế nào đi nữa cũng không thể gọi system.
* Nếu như memcmp so sánh với độ dài là 0 thì sao? (v3).

### strlen()

```
SYNOPSIS         top

       #include <string.h>

       size_t strlen(const char *s);
DESCRIPTION         top

       The strlen() function calculates the length of the string pointed to
       by s, excluding the terminating null byte ('\0').
RETURN VALUE         top

       The strlen() function returns the number of characters in the string
       pointed to by s.
```

* Đại loại là : Hàm strlen() tính toán độ dài string cho đến khi gặp null byte thì kết thúc.
* Như vậy ta có thể truyền  null byte. Khi đó v3=0

### Thử và Cat Flag 
[Exploit](exploit.py)